import { Injectable } from '@nestjs/common';
import { User } from '@entity/user.entity';
import { Connection, Repository } from 'typeorm';
import { CustomLogger } from '@app/logger/customLogger.service';
import { ApiResponse } from '@app/common/apiResponse';
import Error from '@app/common/error';
import { UserInfo } from '@common/user/type';

@Injectable()
export class UserService {
  private readonly logger = new CustomLogger(UserService.name);
  private dbRespository: Repository<User>;
  constructor(private connection: Connection) {
    this.dbRespository = this.connection.getRepository(User);
  }

  async updateUserLoginCount(user: User): Promise<User> {
    user.loginCount += 1;
    return user.save();
  }

  async findUserByName(userName: string): Promise<User | undefined> {
    return this.dbRespository.findOne({
      userName,
    });
  }

  async insertUser(user: User) {
    return user.save();
  }
}
