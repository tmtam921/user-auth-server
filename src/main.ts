import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  await app.listen(3000).then(() => {
    console.log(
      `---- server listening on port: ${process.env.SERVER_LISTEN_PORT} ----`,
    );
  });
}

bootstrap().catch((err) => {
  console.log('---- server start error:', err);
});
