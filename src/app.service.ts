import { Injectable } from '@nestjs/common';
import { CustomLogger } from './logger/customLogger.service';

@Injectable()
export class AppService {
  private readonly logger = new CustomLogger(AppService.name);

  getHello(): string {
    this.logger.log('---- get hello call ----');
    return 'Hello World!';
  }
}
