import { User, UserType } from '@app/entity/user.entity';

export type UserInfo = {
  id: number;
  userName: string;
  isActive: boolean;
  regDate: Date;
  lastUpdateOn: Date;
  loginCount: number;
  type: UserType;
};

export type UserLogin = {
  accessToken: string;
  user: User;
};

export type UserSign = {
  id: number;
  isActived: boolean;
  loginCount: number;
  userName: string;
  enable: boolean;
  lastUpdateOn: Date;
  type: UserType;
};
