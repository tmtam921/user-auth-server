export type ApiResponse<T> = {
  success: boolean;
  data: T;
};

export type UserAuthPayload = {
  userName: string;
  inputPassword: string;
};
