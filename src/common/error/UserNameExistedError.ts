import { ErrorType } from './ErrorType';

export class UserNameExistedError extends Error {
  public data: any;
  constructor(data?: any) {
    super(ErrorType.USER_NAME_EXISTED);
    this.data = data;
  }
}
