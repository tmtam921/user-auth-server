import { ErrorType } from './ErrorType';

export class WrongPasswordError extends Error {
  public data: any;
  constructor(data?: any) {
    super(ErrorType.WRONG_PASSWORD);
    this.data = data;
  }
}
