import { ErrorType } from './ErrorType';

export class UserNotExistedError extends Error {
  public data: any;
  constructor(data?: any) {
    super(ErrorType.USER_NOT_EXISTED);
    this.data = data;
  }
}
