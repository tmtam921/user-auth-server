import { ErrorType } from './ErrorType';

export class UnexpectedError extends Error {
  public data: any;
  constructor(data?: any) {
    super(ErrorType.UNEXPECTED);
    this.data = data;
  }
}
