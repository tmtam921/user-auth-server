export enum ErrorType {
  USER_NAME_EXISTED = 'UserNameExisted',
  UNEXPECTED = 'Unexpected',
  WRONG_PASSWORD = 'WrongPassword',
  USER_NOT_EXISTED = 'UserNotExisted',
}
