import { UnexpectedError } from './UnexpectedError';
import { UserNameExistedError } from './UserNameExistedError';
import { WrongPasswordError } from './WrongPasswordError';

export default {
  UnexpectedError,
  UserNameExistedError,
  WrongPasswordError,
};
