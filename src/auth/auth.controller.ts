import { ApiResponse, UserAuthPayload } from '@app/common/apiResponse';
import { UserLogin } from '@app/common/user/type';
import { User } from '@app/entity/user.entity';
import { CustomLogger } from '@app/logger/customLogger.service';
import {
  Body,
  Post,
  UseGuards,
  UseInterceptors,
  Req,
  Get,
  Res,
} from '@nestjs/common';
import { Controller } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserService } from '@user/user.service';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import LoginDto from './dto/login.dto';
import RegisterDto from './dto/register.dto';
import { JwtAuthGuard } from './guard/jwt.guard';
// import { UserAuthGuard } from './guard/user.guard';

@Controller('auth')
export class AuthController {
  private readonly logger = new CustomLogger(AuthController.name);

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('/login')
  async login(
    @Body() body: LoginDto,
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ): Promise<ApiResponse<UserLogin | Error>> {
    this.logger.log(`---- Post login ${body.userName} ----`);
    console.log('---- request', req.headers);
    const result = await this.authService.validateUserLogin(
      body.userName,
      body.password,
    );

    if (result.success) {
      res.cookie(
        'accessToken',
        (result as unknown as ApiResponse<UserLogin>).data.accessToken,
      );
    }
    return result;
  }

  @Post('/register')
  @UseInterceptors(FileInterceptor('file'))
  register(@Body() body: RegisterDto) {
    // return this.userService.insertUser(body.userName, body.password);
    this.logger.log(`---- Post register ${body.userName} ----`);
    return this.authService.userRegister(body.userName, body.password);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/info')
  async getInfo(@Req() req: Request) {
    console.log('---- hii info', req.user);
    return req.user;
  }
}
