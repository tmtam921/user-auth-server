// import { Strategy } from 'passport-local';
// import { PassportStrategy } from '@nestjs/passport';
// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { User } from '@app/entity/user.entity';
// import { CustomLogger } from '@app/logger/customLogger.service';
// import { AuthService } from '../auth.service';
// import { UserService } from '@user/user.service';
// import { UserNotExistedError } from '@app/common/error/UserNotExistedError';
// import { WrongPasswordError } from '@app/common/error/WrongPasswordError';

// @Injectable()
// export class UserStrategy extends PassportStrategy(Strategy, 'user') {
//   private readonly logger = new CustomLogger(UserStrategy.name);
//   constructor(
//     private authService: AuthService,
//     private userService: UserService,
//   ) {
//     super();
//   }

//   async validate(username: string, password: string): Promise<any> {
//     console.log('---- validate');
//     this.logger.log('----- validate');
//     this.logger.log(username, password);
//     let user: User;
//     try {
//       user = await this.userService.findUserByName(username);
//       console.log('---- hii ', user);
//     } catch (err) {
//       throw err;
//     }
//     if (!user) {
//       throw new UnauthorizedException();
//     }

//     const result = await this.authService.validateUserByPassword(
//       user,
//       password,
//     );
//     if (!result) {
//       return {
//         success: false,
//         data: new WrongPasswordError(result),
//       };
//     }
//     return {
//       success: true,
//       data: user,
//     };
//   }
// }
