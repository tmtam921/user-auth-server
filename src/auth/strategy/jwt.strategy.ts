import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '@user/user.service';
import { UserNotExistedError } from '@app/common/error/UserNotExistedError';
import { User } from '@app/entity/user.entity';
import { WrongPasswordError } from '@app/common/error/WrongPasswordError';
import { AuthService } from '../auth.service';
import { ApiResponse } from '@app/common/apiResponse';
import { UserSign } from '@app/common/user/type';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(user: UserSign): Promise<User> {
    const updateUser = this.userService.findUserByName(user.userName);
    if (!updateUser) {
      throw new UnauthorizedException();
    }
    return updateUser;
  }
}
