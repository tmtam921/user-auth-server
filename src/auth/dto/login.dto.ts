export default class LoginDto {
  readonly userName: string;
  readonly password: string;
}
