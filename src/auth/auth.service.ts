import { JwtService } from '@nestjs/jwt';
import { ApiResponse } from '@app/common/apiResponse';
import { UserNotExistedError } from '@app/common/error/UserNotExistedError';
import { WrongPasswordError } from '@app/common/error/WrongPasswordError';
import { User } from '@app/entity/user.entity';
import { CustomLogger } from '@app/logger/customLogger.service';
import { Injectable } from '@nestjs/common';
import { UserService } from '@user/user.service';
import * as bcrypt from 'bcrypt';
import { UserLogin, UserSign } from '@app/common/user/type';
import { UserNameExistedError } from '@app/common/error/UserNameExistedError';
import { UnexpectedError } from '@app/common/error/UnexpectedError';

@Injectable()
export class AuthService {
  private readonly logger = new CustomLogger(AuthService.name);

  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUserByPassword(
    user: User,
    inputPassword: string,
  ): Promise<boolean> {
    this.logger.log('---- validateUserByPassword ----');
    const result = await bcrypt.compare(inputPassword, user.password);
    if (!result) {
      throw new WrongPasswordError(result);
    }
    return result;
  }

  async jwtSign(useSign: UserSign): Promise<string> {
    return this.jwtService.sign(useSign);
  }

  async validateUserLogin(
    userName: string,
    inputPassword: string,
  ): Promise<ApiResponse<UserLogin | Error>> {
    let user: User;
    try {
      user = await this.userService.findUserByName(userName);
      if (!user) {
        throw user;
      }
    } catch (err) {
      return {
        success: false,
        data: new UserNotExistedError(err),
      };
    }

    try {
      await this.validateUserByPassword(user, inputPassword);
    } catch (err) {
      return {
        success: false,
        data: err,
      };
    }

    const { password, tempPassword, regDate, ...signData } = user;
    const token = await this.jwtSign(signData);

    this.userService.updateUserLoginCount(user);
    return {
      success: true,
      data: {
        accessToken: token,
        user,
      },
    };
  }

  async userRegister(
    userName: string,
    password: string,
  ): Promise<ApiResponse<User | Error>> {
    const isExist = await this.userService.findUserByName(userName);
    if (isExist) {
      return {
        success: false,
        data: new UserNameExistedError(),
      };
    }
    const user = new User();
    user.userName = userName;
    user.password = password;
    try {
      const newUser = await this.userService.insertUser(user);
      return {
        success: true,
        data: newUser,
      };
    } catch (err) {
      return {
        success: false,
        data: new UnexpectedError(err),
      };
    }
  }
}
