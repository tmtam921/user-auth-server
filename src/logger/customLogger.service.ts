import {
  Injectable,
  Logger as AppLogger,
  LoggerService,
  LogLevel,
  Scope,
} from '@nestjs/common';
import * as log4js from 'log4js';

@Injectable({
  scope: Scope.TRANSIENT,
})
export class CustomLogger extends AppLogger {
  private log4js: log4js.Logger;

  constructor(prefix: string) {
    super(prefix, { timestamp: true });
    log4js.configure({
      appenders: {
        file: {
          filename: process.env.LOG_FILE_PATH,
          type: 'file',
          layout: { type: 'pattern', pattern: '%d [%p] %m' },
          pattern: 'yyyy-MM-dd',
          keepFileExt: true,
          alwaysIncludePattern: true,
          daysToKeep: 10,
        },
      },
      categories: {
        default: {
          appenders: ['file'],
          level: 'all',
        },
      },
    });
    this.log4js = log4js.getLogger();
  }

  error(...rest: (string | any[])[]): void {
    super.error(rest);
    this.log4js?.error(rest);
  }

  log(...rest: (string | any[])[]): void {
    super.log(rest);
    this.log4js?.info(rest);
  }

  warn(...rest: (string | any[])[]): void {
    super.warn(rest);
    this.log4js?.warn(rest);
  }

  debug(...rest: (string | any[])[]): void {
    super.debug(rest);
    this.log4js?.debug(rest);
  }

  verbose(...rest: (string | any[])[]): void {
    super.verbose(rest);
    this.log4js?.info(rest);
  }
}
