import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  AfterLoad,
} from 'typeorm';
import * as bcrypt from 'bcrypt';

export enum UserType {
  User = 'user',
  Admin = 'admin',
}

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', {
    length: 32,
    unique: true,
  })
  userName: string;

  @Column('varchar', {
    length: 255,
  })
  password: string;

  @Column('boolean', {
    default: 0,
  })
  isActived: boolean;

  @Column('boolean', {
    default: 0,
  })
  enable: boolean;

  @CreateDateColumn()
  regDate: Date;

  @UpdateDateColumn()
  lastUpdateOn: Date;

  @Column('int', {
    default: 0,
  })
  loginCount: number;

  tempPassword: string;

  @Column({
    type: 'enum',
    enum: UserType,
    default: UserType.User,
  })
  type: UserType;

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password;
  }

  @BeforeInsert()
  @BeforeUpdate()
  async setPassword(password: string) {
    if (this.tempPassword !== this.password) {
      const salt = await bcrypt.genSalt();
      this.password = await bcrypt.hash(password || this.password, salt);
    }
  }
}
