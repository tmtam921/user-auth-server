FROM node:14 AS builder
WORKDIR /usr/src/app/
COPY ./ ./

FROM builder AS auth-server-dev
RUN yarn install --frozen-lockfile --network-timeout 1000000
CMD ["yarn", "run", "start:dev"]

FROM auth-server-dev AS auth-server
RUN yarn run build
EXPOSE 3000
CMD ["node", "./dist/main"]
